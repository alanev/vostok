(function () {
    var carousels = document.querySelectorAll('.carousel');
    
    $('.carousel').owlCarousel({
        navigation: true,
        navigationText: ['<div class="i-arrow-prev"></div>','<div class="i-arrow-next"></div>'],
        pagination: false,
        singleItem: true,
        mouseDrag : false
    });
})();