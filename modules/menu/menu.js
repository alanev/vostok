(function () {
    var menu = {
        more: document.querySelector('.menu__more'),
        dropdown: document.querySelector('.menu__dropdown'),
        close: document.querySelector('.menu__close'),
        toggle () {
            this.more.classList.toggle('_active');
            this.dropdown.classList.toggle('_active');
            document.body.classList.toggle('_lock');
        },
        init () {
            this.more.addEventListener('click', function () {
                menu.toggle();
            });
            this.close.addEventListener('click', function () {
                menu.toggle();
            });
        }
    };
    menu.init();
})();