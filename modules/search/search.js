(function () {
    var search = {
        module: document.querySelector('.search'),
        button: document.querySelector('.search__icon'),
        input: document.querySelector('.search__input'),
        toggle () {
            this.module.classList.toggle('_active');
        },
        init () {
            this.button.addEventListener('click', function () {
                search.toggle();
            });
        }
    };
    search.init();
})();