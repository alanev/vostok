(function () {
    // var map = document.querySelector('.map__fram');
    ymaps.ready(init);
    
    var map, placemark;
    var center = [57.136333, 65.574411];
    
    function init () {
        map = new ymaps.Map('map', {
            center: center,
            zoom: 12
        });
        placemark = new ymaps.Placemark(center, {}, {
            iconLayout: 'default#image',
            iconImageHref: 'img/placemark.png',
            iconImageSize: [35, 46],
            iconImageOffset: [-17, -46]
        });
        
        map.geoObjects.add(placemark);
    }
})();